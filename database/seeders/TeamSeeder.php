<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'name' => 'Manchester City',
            'short_name' => 'MCI',
            'squad_power' => 95,
        ]);

        DB::table('teams')->insert([
            'name' => 'Arsenal',
            'short_name' => 'ARS',
            'squad_power' => 85,
        ]);

        DB::table('teams')->insert([
            'name' => 'Liverpool',
            'short_name' => 'LVP',
            'squad_power' => 75,
        ]);

        DB::table('teams')->insert([
            'name' => 'Chelsea',
            'short_name' => 'CHE',
            'squad_power' => 65,
        ]);
    }
}
