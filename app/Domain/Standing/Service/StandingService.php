<?php

namespace App\Domain\Standing\Service;

use App\Domain\Standing\Repository\Contacts\StandingRepositoryInterface;

class StandingService
{
    private StandingRepositoryInterface $standingRepository;

    public function __construct(StandingRepositoryInterface $standingRepository)
    {
        $this->standingRepository = $standingRepository;
    }

    public function initStandings(array $teams): void
    {
        foreach ($teams as $team) {
            $this->standingRepository->createInitialStanding($team['id']);
        }
    }

    public function getStandings(): array
    {
        $standings = $this->standingRepository->getStandings();
        $result = [];
        foreach ($standings as $standing) {
            $result[] = [
                'team_name' => $standing->team->name,
                'points' => $standing->points,
                'win' => $standing->win,
                'drawn' => $standing->drawn,
                'lost' => $standing->lost,
                'goal_diff' => $standing->goals_for - $standing->goals_against
            ];
        }

        return $result;
    }

    public function isFixtureArranged(): bool
    {
        return $this->standingRepository->isFixtureArranged();
    }

    public function clearStandings(): void
    {
        $this->standingRepository->removeAll();
    }
}
