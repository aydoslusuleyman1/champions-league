<?php

namespace App\Domain\Standing\Model;

use App\Domain\Team\Model\Team;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $id
 */
class Standing extends Model
{
    use HasFactory;

    protected $table = 'standings';

    protected $guarded = [];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }
}
