<?php

namespace App\Domain\GameStatistics\Service\GameResultHandler;

use App\Domain\Core\Enum\TeamTypeEnum;

class GameResultHandler
{
    private ?GameResultHandlerInterface $strategy = null;

    public function __construct($type)
    {
        switch ($type) {
            case TeamTypeEnum::HOME_TEAM :
                $this->strategy = new HomeTeamResultHandler();
                break;
            case TeamTypeEnum::AWAY_TEAM :
                $this->strategy = new AwayTeamResultHandler();
                break;
        }
    }

    public function init(array $data)
    {
        return $this->strategy->handle($data);
    }
}
