<?php

namespace App\Domain\GameStatistics\Service\GameResultHandler;

use App\Domain\Core\Enum\GameResultEnum;
use App\Domain\GameStatistics\Helper\GameStatisticsHelper;
use App\Domain\GameStatistics\Repository\Contracts\GameStatisticsRepositoryInterface;
use App\Domain\Standing\Repository\Contacts\StandingRepositoryInterface;

class AwayTeamResultHandler implements GameResultHandlerInterface
{
    public function handle(array $data)
    {
        $gameStatisticsRepository = app(GameStatisticsRepositoryInterface::class);
        $status = GameStatisticsHelper::getMatchResult($data['away_score'], $data['home_score']);
        $awayTeamData = [
            'game_id' => $data['game_id'],
            'team_id' => $data['away_team_id'],
            'score' => $data['away_score'],
            'red_card' => $data['away_red_card'],
            'status' => $status
        ];

        $gameStatisticsRepository->insertGameStatisticByTeam($awayTeamData);

        $standingRepository = app(StandingRepositoryInterface::class);
        $awayTeamStandingData = $standingRepository->getTeamStanding($data['away_team_id']);

        $standingData = [
            'team_id' => $data['away_team_id'],
            'points' => $awayTeamStandingData->points + GameStatisticsHelper::getPointsByStatus($status),
            'goals_for' => $awayTeamStandingData->goals_for + $data['away_score'],
            'goals_against' => $awayTeamStandingData->goals_against + $data['home_score'],
            'win' => $status == GameResultEnum::WIN ? $awayTeamStandingData->win + 1 : $awayTeamStandingData->win,
            'drawn' => $status == GameResultEnum::DRAWN ? $awayTeamStandingData->drawn + 1 : $awayTeamStandingData->drawn,
            'lost' => $status == GameResultEnum::LOST ? $awayTeamStandingData->lost + 1 : $awayTeamStandingData->lost,
        ];

        $standingRepository->updateStandingByTeamId($standingData);
    }
}
