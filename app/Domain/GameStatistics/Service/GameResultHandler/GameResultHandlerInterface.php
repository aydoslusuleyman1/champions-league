<?php

namespace App\Domain\GameStatistics\Service\GameResultHandler;

interface GameResultHandlerInterface
{
    public function handle(array $data);
}
