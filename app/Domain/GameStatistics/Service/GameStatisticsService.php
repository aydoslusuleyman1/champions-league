<?php

namespace App\Domain\GameStatistics\Service;

use App\Domain\GameStatistics\Repository\Contracts\GameStatisticsRepositoryInterface;

class GameStatisticsService
{
    private GameStatisticsRepositoryInterface $gameStatisticsRepository;

    public function __construct(GameStatisticsRepositoryInterface $gameStatisticsRepository)
    {
        $this->gameStatisticsRepository = $gameStatisticsRepository;
    }

    public function isGamePlayed(int $gameId): bool
    {
        return $this->gameStatisticsRepository->checkGamePlayedByGameId($gameId);
    }

    public function isWeeklyGamesPlayed(int $weekId): bool
    {
        return $this->gameStatisticsRepository->checkIsGamesPlayedByWeek($weekId);
    }

    public function getLastPlayedGame()
    {
        return $this->gameStatisticsRepository->getLastPlayedGame();
    }

    public function removeAllStatistics(): void
    {
        $allItems = $this->gameStatisticsRepository->getAll();
        foreach ($allItems as $item) {
            $item->delete();
        }
    }
}
