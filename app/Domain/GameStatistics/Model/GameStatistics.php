<?php

namespace App\Domain\GameStatistics\Model;

use App\Domain\Game\Model\Game;
use App\Domain\Team\Model\Team;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $id
 */
class GameStatistics extends Model
{
    use HasFactory;

    protected $table = 'game_statistics';

    protected $guarded = [];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }

    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id', 'id');
    }
}
