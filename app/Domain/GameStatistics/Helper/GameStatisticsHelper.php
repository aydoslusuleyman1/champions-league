<?php

namespace App\Domain\GameStatistics\Helper;

use App\Domain\Core\Enum\GameResultEnum;

class GameStatisticsHelper
{
    public static function getMatchResult($goalsFor, $goalsAgainst): int
    {
        $status = GameResultEnum::DRAWN;
        if ($goalsFor > $goalsAgainst) {
            $status = GameResultEnum::WIN;
        } else if ($goalsFor < $goalsAgainst) {
            $status = GameResultEnum::LOST;
        }

        return $status;
    }

    public static function getPointsByStatus($status): int
    {
        $pointsMap = [
            GameResultEnum::DRAWN => 1,
            GameResultEnum::WIN => 3,
            GameResultEnum::LOST => 0
        ];

        return $pointsMap[$status];
    }
}
