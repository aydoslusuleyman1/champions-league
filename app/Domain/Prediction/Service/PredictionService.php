<?php

namespace App\Domain\Prediction\Service;

use App\Domain\Game\Service\GameService;
use App\Domain\GameStatistics\Service\GameStatisticsService;
use App\Domain\Standing\Repository\Contacts\StandingRepositoryInterface;
use App\Domain\Team\Service\TeamService;

class PredictionService
{
    private StandingRepositoryInterface $standingRepository;
    private GameService $gameService;
    private TeamService $teamService;

    private GameStatisticsService $gameStatisticsService;

    public function __construct(StandingRepositoryInterface $standingRepository, GameService $gameService, TeamService $teamService, GameStatisticsService $gameStatisticsService)
    {
        $this->standingRepository = $standingRepository;
        $this->gameService = $gameService;
        $this->teamService = $teamService;
        $this->gameStatisticsService = $gameStatisticsService;
    }

    public function calculatePrediction(): array
    {
        $maxWeek = $this->gameService->getMaxWeekIndex();
        $lastWeek = $this->gameStatisticsService->getLastPlayedGame();
        $remainingWeeks = $maxWeek - ($lastWeek ? $lastWeek->game->week : 0);
        $maxRemainingPoints = $remainingWeeks * 3;
        $teams = $this->teamService->getAllTeams();

        $result = [];
        if ($remainingWeeks > 3) {
            foreach ($teams as $team) {
                $result[$team->name] = 0;
            }
            return $result;
        }

        $teamStandings = $this->standingRepository->getStandings();
        $firstTeam = $teamStandings->get(0);
        $secondTeam = $teamStandings->get(1);

        $pointDiff = $firstTeam->points - $secondTeam->points;
        if ($maxRemainingPoints < $pointDiff || $maxRemainingPoints ==0) {
            $championTeamId = $firstTeam->team_id;
            foreach ($teams as $team) {
                $result[$team->name] = $championTeamId == $team->id ? 100 : 0;
            }
            arsort($result);
            return $result;
        }

        foreach ($teams as $team) {
            $teamStanding = $this->standingRepository->getTeamStanding($team->id);
            $teamCurrentPoints = $teamStanding->points;
            $maxReachablePoints = $teamCurrentPoints + $maxRemainingPoints;
            $result[$team->name] = $maxReachablePoints;
        }

        $totalPoints = array_sum($result);
        $list = [];
        foreach ($result as $key => $team) {
            $list[$key] = number_format($team / $totalPoints * 100, 2);
        }
        arsort($list);

        return $list;
    }
}
