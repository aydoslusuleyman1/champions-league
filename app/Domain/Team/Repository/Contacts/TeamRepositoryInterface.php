<?php

namespace App\Domain\Team\Repository\Contacts;

interface TeamRepositoryInterface
{
    public function getAllTeams();
}
