<?php

namespace App\Domain\Team\Repository;

use App\Domain\Team\Model\Team;
use App\Domain\Team\Repository\Contacts\TeamRepositoryInterface;

class TeamRepository implements TeamRepositoryInterface
{
    private Team $model;

    /**
     * TeamRepository constructor.
     */
    public function __construct(Team $model)
    {
        $this->model = $model;
    }

    public function getAllTeams()
    {
        return $this->model->all();
    }
}
