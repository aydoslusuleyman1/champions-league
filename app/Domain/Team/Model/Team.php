<?php

namespace App\Domain\Team\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $id
 */
class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';
}
