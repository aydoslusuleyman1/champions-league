<?php

namespace App\Domain\Game\Repository\Contacts;

use App\Domain\Team\Model\Team;

interface GameRepositoryInterface
{
    public function getAll();

    public function saveGame(int $homeTeamId, int $awayTeamId, int $weekIndex);

    public function getGames();

    public function getGamesByWeekId(int $weekId, bool $getFurther);

    public function getMaxWeekIndex(): int;
}
