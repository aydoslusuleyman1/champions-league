<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Prediction\Service\PredictionService;
use App\Domain\Standing\Service\StandingService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class StandingController extends Controller
{
    public function getStandings(StandingService $standingService): JsonResponse
    {
        return response()->json(['standings' => $standingService->getStandings()]);
    }

    public function getPredictions(PredictionService $predictionService): JsonResponse
    {
        return response()->json(['predictions' => $predictionService->calculatePrediction()]);
    }
}
