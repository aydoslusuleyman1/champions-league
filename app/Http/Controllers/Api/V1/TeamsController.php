<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Team\Service\TeamService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class TeamsController extends Controller
{
    public function getTeams(TeamService $teamService): JsonResponse
    {
        return response()->json($teamService->getAllTeams());
    }
}
