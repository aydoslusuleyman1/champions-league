<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Game\Service\GameService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class SimulationController extends Controller
{
    public function playGames(GameService $gameService, int $weekId, bool $playAll = false): JsonResponse
    {
        $gameService->simulateGames($weekId, $playAll);
        return response()->json(['status' => true]);
    }
}
