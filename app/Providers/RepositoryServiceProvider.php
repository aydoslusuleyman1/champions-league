<?php

namespace App\Providers;

use App\Domain\Game\Repository\Contacts\GameRepositoryInterface;
use App\Domain\Game\Repository\GameRepository;
use App\Domain\GameStatistics\Repository\Contracts\GameStatisticsRepositoryInterface;
use App\Domain\GameStatistics\Repository\GameStatisticsRepository;
use App\Domain\Standing\Repository\Contacts\StandingRepositoryInterface;
use App\Domain\Standing\Repository\StandingRepository;
use App\Domain\Team\Repository\Contacts\TeamRepositoryInterface;
use App\Domain\Team\Repository\TeamRepository;
use Carbon\Laravel\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GameRepositoryInterface::class, GameRepository::class);
        $this->app->bind(GameStatisticsRepositoryInterface::class, GameStatisticsRepository::class);
        $this->app->bind(StandingRepositoryInterface::class, StandingRepository::class);
        $this->app->bind(TeamRepositoryInterface::class, TeamRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
