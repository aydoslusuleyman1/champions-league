<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    Route::prefix('teams')->group(function () {
        Route::get('/get', [\App\Http\Controllers\Api\V1\TeamsController::class, 'getTeams']);
    });

    Route::prefix('schedule')->group(function () {
        Route::get('/generate', [\App\Http\Controllers\Api\V1\ScheduleController::class, 'generate']);
        Route::get('/getWeek/{id}', [\App\Http\Controllers\Api\V1\ScheduleController::class, 'getWeek']);
        Route::get('/reset', [\App\Http\Controllers\Api\V1\ScheduleController::class, 'resetFixture']);
    });

    Route::prefix('games')->group(function () {
        Route::get('/get', [\App\Http\Controllers\Api\V1\GamesController::class, 'getGames']);
        Route::get('/get-week-games/{week}', [\App\Http\Controllers\Api\V1\GamesController::class, 'getWeekGames']);
    });

    Route::prefix('standings')->group(function () {
        Route::get('/get', [\App\Http\Controllers\Api\V1\StandingController::class, 'getStandings']);
        Route::get('/predictions', [\App\Http\Controllers\Api\V1\StandingController::class, 'getPredictions']);
    });

    Route::prefix('simulation')->group(function () {
        Route::get('/play-games/{week}/{playAll?}', [\App\Http\Controllers\Api\V1\SimulationController::class, 'playGames']);
    });

    Route::get('/config', [\App\Http\Controllers\Api\V1\ConfigController::class, 'index']);
    Route::get('/config-simulation/{week}', [\App\Http\Controllers\Api\V1\ConfigController::class, 'simulation']);
});

